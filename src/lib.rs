mod utils;

use std::collections::HashMap;
use wasm_bindgen::prelude::*;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Permap {
    store: HashMap<String, String>,
}

#[wasm_bindgen]
impl Permap {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        utils::set_panic_hook();
        Self {
            store: HashMap::new(),
        }
    }

    pub fn get(&self, key: &str) -> Option<String> {
        if let Some(value) = self.store.get(key) {
            return Some(value.to_owned());
        }
        None
    }

    pub fn set(&mut self, key: String, value: String) {
        self.store.insert(key, value);
    }
}
