> This is WIP. No, the only I've written isn't a Rust HashMap.

# Permap

Persistent, performant JavaScript maps. Permap has a near equivalent API to the standard JS `Map`, with the only main differences being that

- you can only insert string values:
    ```js
    pm.set("myKey", JSON.stringify({
        hello: "world"
    }));
    ```
- a driver must be passed to the constructor:
    ```js
    const pm = new Permap(new JsonDriver("./db.json"));
    ```

## Getting Started

First, install the npm package:

```console
$ npm i permap # yarn add permap
```

Once this is done, you should be able to import it:

```js
const { Permap } = require("permap");
// or with es2015 imports
import { Permap } from "permap";
```
